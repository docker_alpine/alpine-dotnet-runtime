# alpine-dotnet-runtime
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-dotnet-runtime)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-dotnet-runtime)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-dotnet-runtime/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-dotnet-runtime/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [.NET Core](https://github.com/dotnet/core/)
    - .NET Core is a free and open-source managed computer software framework for the Windows, Linux, and macOS operating systems.



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/alpine-dotnet-runtime:[ARCH_TAG]
```



----------------------------------------
#### Usage

* Runtime



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

